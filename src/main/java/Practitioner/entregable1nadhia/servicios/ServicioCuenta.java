package Practitioner.entregable1nadhia.servicios;

import Practitioner.entregable1nadhia.modelos.Cliente;
import Practitioner.entregable1nadhia.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    // CRUD
    public List<Cuenta> obtenerCuentas();

    // CREATE
    public void insertarCuentaNueva(Cuenta cuenta);

    // READ
    public Cuenta obtenerCuenta(String numero);

    // UPDATE (solamente modificar, no crear).
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    // DELETE
    public void borrarCuenta(String numero);


}