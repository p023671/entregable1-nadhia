package Practitioner.entregable1nadhia.servicios.impl;

import Practitioner.entregable1nadhia.modelos.Cliente;
import Practitioner.entregable1nadhia.modelos.Cuenta;
import Practitioner.entregable1nadhia.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String, Cuenta> cuentas = new ConcurrentHashMap<String,Cuenta>();

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.cuentas.put(cuenta.numero, cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if (!this.cuentas.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);
        return this.cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if (!this.cuentas.containsKey(cuenta.numero))
            throw new RuntimeException("No existe la cuenta: " + cuenta.numero);

        // "Piso" el objeto en el hashmap.
        this.cuentas.replace(cuenta.numero, cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        final Cuenta existente = this.cuentas.get(parche.numero);

        if(parche.numero != existente.numero)
            existente.numero = parche.numero;

        if(parche.moneda != null)
            existente.moneda = parche.moneda;

        if(parche.saldo != null)
            existente.saldo = parche.saldo;

        if(parche.tipo != null)
            existente.tipo = parche.tipo;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if(parche.oficina != null)
            existente.oficina = parche.oficina;

        this.cuentas.replace(existente.numero, existente);
    }

    @Override
    public void borrarCuenta(String numero) {
        if (!this.cuentas.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);
        this.cuentas.remove(numero);
    }

}