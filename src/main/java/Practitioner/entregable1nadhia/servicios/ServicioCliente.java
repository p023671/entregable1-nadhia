package Practitioner.entregable1nadhia.servicios;

import Practitioner.entregable1nadhia.modelos.Cliente;
import Practitioner.entregable1nadhia.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {

    // CRUD
    public List<Cliente> obtenerClientes();

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);

    // DELETE
    public void borrarCliente(String documento);


    public void agregarCuentaCliente(String documento, Cuenta cuenta);

    public List<Cuenta> obtenerCuentasCliente(String documento);


}
