package Practitioner.entregable1nadhia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Entregable1NadhiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Entregable1NadhiaApplication.class, args);
	}

}
