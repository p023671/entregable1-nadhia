package Practitioner.entregable1nadhia.controlador;

import org.springframework.beans.factory.annotation.Value;

public class Rutas {
    public static final String BASE = "/api/v1";
    public static final String CLIENTES = BASE + "/clientes";
    public static final String CUENTAS = BASE + "/cuentas";
}
