package Practitioner.entregable1nadhia.controlador;

import Practitioner.entregable1nadhia.modelos.Cuenta;
import Practitioner.entregable1nadhia.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // GET http://localhost:9000/api/v1/clientes -> List<Cliente> obtenerClientes()
    @GetMapping
    public List<Cuenta> obtenerCuentas() {
        return this.servicioCuenta.obtenerCuentas();
    }

    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta) {
        this.servicioCuenta.insertarCuentaNueva(cuenta);
    }

    // GET http://localhost:9000/api/v1/clientes/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientes/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{numero}")
    public Cuenta obtenerUnaCuenta(@PathVariable String numero) {
        try {
            return this.servicioCuenta.obtenerCuenta(numero);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> reemplazarUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{numero}")
    public void reemplazarUnaCuenta(@PathVariable("numero") String nroCta,
                                    @RequestBody Cuenta cuenta) {
        try {
            cuenta.numero = nroCta;
            this.servicioCuenta.guardarCuenta(cuenta);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{numero}")
    public void emparacharUnaCuenta(@PathVariable("numero") String nroCta,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numero = nroCta;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    // DELETE http://localhost:9000/api/v1/clientes/{documento} -> eliminarUnCliente(documento)
    // DELETE http://localhost:9000/api/v1/clientes/12345678 + DATOS -> eliminarUnCliente("12345678")
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public void eliminarUnaCuenta(@PathVariable String numero) {
        try {
            this.servicioCuenta.borrarCuenta(numero);
        } catch(Exception x) {}
    }
}
